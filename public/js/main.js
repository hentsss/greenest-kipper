document.addEventListener("DOMContentLoaded", event => {
  const app = firebase.app();
  const db = firebase.firestore();
  const txtEmail = document.getElementById("txtEmail");
  const txtPassword = document.getElementById("txtPassword");
  const btnLogin = document.getElementById('btnLogin');
  const btnLogout = document.getElementById('btnLogout');
  const adminContent = document.getElementById('adminContent');

  btnLogin.addEventListener('click', e => {
    const email = txtEmail.value;
    const pass = txtPassword.value;
    const auth = firebase.auth();
    
    const promise = auth.signInWithEmailAndPassword(email, pass);
    promise.catch(e => console.log(e.message));

  })

  btnLogout.addEventListener('click', e => {
    firebase.auth().signOut();
  })

  firebase.auth().onAuthStateChanged(firebaseUser => {
    if (firebaseUser) {
      console.log(firebaseUser);
      btnLogout.classList.remove('hidden');
      adminContent.classList.remove('hidden');
    } else {
      console.log("Not signed in!");
      btnLogout.classList.add('hidden');
      adminContent.classList.add('hidden');
    }
  })

  db.collection('projects').get()
  .then((snapshot) => {
    snapshot.docs.forEach(doc => {
      getProjects(doc);
  })
})

db.collection('materials').get()
.then((snapshot) => {
  snapshot.docs.forEach(doc => {
    getMaterials(doc);
})
})

db.collection('roofing').get()
.then((snapshot) => {
  snapshot.docs.forEach(doc => {
    getRoofing(doc);
})
})

db.collection('cladding').get()
.then((snapshot) => {
  snapshot.docs.forEach(doc => {
    getCladding(doc);
})
})

});


const form = document.getElementById("add-project");
form.addEventListener('submit', (e) => {
  e.preventDefault();
  var image = form.image.files;
  const db = firebase.firestore();
  const storageRef = firebase.storage().ref();
  const image1Ref = storageRef.child(image.item(0).name);
  const file = image.item(0);
  const task = image1Ref.put(file);
  
  task.then(snapshot => {
    snapshot.ref.getDownloadURL().then(function(downloadURL) {
      var post = db.collection("projects");
      post.doc(form.name.value).set({
        name: form.name.value,
        materials: form.materials.value,
        systems: form.systems.value,
        techs: form.techs.value,
        date: form.date.value,
        type: form.type.value,
        image: downloadURL
      })
    });
  })
})

const form2 = document.getElementById("add-material");
form2.addEventListener('submit', (e) => {
  e.preventDefault();
  var image = form2.image.files;
  const db = firebase.firestore();
  const storageRef = firebase.storage().ref();
  const image1Ref = storageRef.child(image.item(0).name);
  const file = image.item(0);
  const task = image1Ref.put(file);
  
  task.then(snapshot => {
    snapshot.ref.getDownloadURL().then(function(downloadURL) {
      var post = db.collection("materials");
      post.doc(form2.name.value).set({
        name: form2.name.value,
        desc: form2.desc.value,
        image: downloadURL
      })
    });
  })
})

const form3 = document.getElementById("add-roofing");
form3.addEventListener('submit', (e) => {
  e.preventDefault();
  var image1 = form3.image1.files;
  var image2 = form3.image2.files;
  var image3 = form3.image3.files;

  const db = firebase.firestore();
  const storageRef = firebase.storage().ref();
  const image1Ref = storageRef.child(image1.item(0).name);
  const image2Ref = storageRef.child(image2.item(0).name);
  const image3Ref = storageRef.child(image3.item(0).name);
  const file1 = image1.item(0);
  const file2 = image2.item(0);
  const file3 = image3.item(0);


  const task1 = image1Ref.put(file1);
  task1.then(snapshot => {
    snapshot.ref.getDownloadURL().then(function(downloadURL) {
      var post = db.collection("roofing");
      post.doc(form3.name.value).set({
        title: form3.name.value,
        content: form3.content.value,
        img1: downloadURL,
        img2: "",
        img3: ""
      })
      setTimeout(secondImg, 5000);
      setTimeout(thirdImg, 3000);
    });
  })
function secondImg() {
  const task2 = image2Ref.put(file2);
  task2.then(snapshot => {
    snapshot.ref.getDownloadURL().then(function(downloadURL) {
      var post = db.collection("roofing");
      post.doc(form3.name.value).update({
        img2: downloadURL
      })
    });
  })
}

function thirdImg() {
  const task3 = image3Ref.put(file3);
  task3.then(snapshot => {
    snapshot.ref.getDownloadURL().then(function(downloadURL) {
      var post = db.collection("roofing");
      post.doc(form3.name.value).update({
        img3: downloadURL
      })
    });
  })
}
})

const form4 = document.getElementById("add-cladding");
form4.addEventListener('submit', (e) => {
  e.preventDefault();
  var image1 = form4.image1.files;
  var image2 = form4.image2.files;
  var image3 = form4.image3.files;

  const db = firebase.firestore();
  const storageRef = firebase.storage().ref();
  const image1Ref = storageRef.child(image1.item(0).name);
  const image2Ref = storageRef.child(image2.item(0).name);
  const image3Ref = storageRef.child(image3.item(0).name);
  const file1 = image1.item(0);
  const file2 = image2.item(0);
  const file3 = image3.item(0);


  const task1 = image1Ref.put(file1);
  task1.then(snapshot => {
    snapshot.ref.getDownloadURL().then(function(downloadURL) {
      var post = db.collection("cladding");
      post.doc(form4.name.value).set({
        title: form4.name.value,
        content: form4.content.value,
        img1: downloadURL,
        img2: "",
        img3: ""
      })
      setTimeout(secondImg, 5000);
      setTimeout(thirdImg, 3000);
    });
  })
function secondImg() {
  const task2 = image2Ref.put(file2);
  task2.then(snapshot => {
    snapshot.ref.getDownloadURL().then(function(downloadURL) {
      var post = db.collection("cladding");
      post.doc(form4.name.value).update({
        img2: downloadURL
      })
    });
  })
}

function thirdImg() {
  const task3 = image3Ref.put(file3);
  task3.then(snapshot => {
    snapshot.ref.getDownloadURL().then(function(downloadURL) {
      var post = db.collection("cladding");
      post.doc(form4.name.value).update({
        img3: downloadURL
      })
    });
  })
}
})

const materials = document.getElementById("materialsList");

function getMaterials(doc) {
  let div = document.createElement('div');
  let h2 = document.createElement('h2');
  let desc = document.createElement('p');
  let image = document.createElement("IMG");
  let cross = document.createElement('div');
  cross.classList.add("delete-button");

  const db = firebase.firestore();
  div.setAttribute('data-id', doc.id);
  desc.textContent = doc.data().desc;
  image.src = doc.data().image;
  image.classList.toggle("materials-image");
  cross.textContent = "Delete material";
  h2.textContent = doc.data().name;

  div.appendChild(h2);
  div.appendChild(image);
  div.appendChild(desc);
  div.appendChild(cross);

  materials.appendChild(div);

  cross.addEventListener('click', (e) => {
    e.stopPropagation();
    let id = e.target.parentElement.getAttribute('data-id');
    db.collection('materials').doc(id).delete();
  })
}

function getProjects(doc){
  let div = document.createElement('div');
  let name = document.createElement('h2');
  let date = document.createElement('p');
  let cross = document.createElement('div');
  cross.classList.add("delete-button");
  const db = firebase.firestore();
  div.setAttribute('data-id', doc.id);
  name.textContent = doc.data().name;
  date.textContent = "Date: " + doc.data().date;
  cross.textContent = "Delete project";

  div.appendChild(name);
  div.appendChild(date);
  div.appendChild(cross);

  projects.appendChild(div);

  cross.addEventListener('click', (e) => {
    e.stopPropagation();
    let id = e.target.parentElement.getAttribute('data-id');
    db.collection('projects').doc(id).delete();
  })
}

const roofing = document.getElementById("roofingList");
function getRoofing(doc){
  let div = document.createElement('div');
  let name = document.createElement('h2');
  let cross = document.createElement('div');
  cross.classList.add("delete-button");
  const db = firebase.firestore();
  div.setAttribute('data-id', doc.id);
  name.textContent = doc.data().title;
  cross.textContent = "Delete solution";

  div.appendChild(name);
  div.appendChild(cross);

  roofing.appendChild(div);

  cross.addEventListener('click', (e) => {
    e.stopPropagation();
    let id = e.target.parentElement.getAttribute('data-id');
    db.collection('roofing').doc(id).delete();
  })
}

const cladding = document.getElementById("claddingList");
function getCladding(doc){
  let div = document.createElement('div');
  let name = document.createElement('h2');
  let cross = document.createElement('div');
  cross.classList.add("delete-button");
  const db = firebase.firestore();
  div.setAttribute('data-id', doc.id);
  name.textContent = doc.data().title;
  cross.textContent = "Delete solution";

  div.appendChild(name);
  div.appendChild(cross);

  cladding.appendChild(div);

  cross.addEventListener('click', (e) => {
    e.stopPropagation();
    let id = e.target.parentElement.getAttribute('data-id');
    db.collection('cladding').doc(id).delete();
  })
}

function updateAboutDesc() {
  const db = firebase.firestore();
  const aboutDesc = db.collection('aboutus').doc('description');
  aboutDesc.update({desc: document.getElementById("about-desc-text").value})
}

function updateMiningDesc() {
  const db = firebase.firestore();
  const miningDesc = db.collection('aboutus').doc('mining');
  miningDesc.update({desc: document.getElementById("about-mining-text").value})
}

function updateLeftManagerDesc() {
  const db = firebase.firestore();
  const leftManager = db.collection('aboutus').doc('manager1');
  leftManager.update({desc: document.getElementById("management-left-textinput").value})
}

function updateRightManagerDesc() {
  const db = firebase.firestore();
  const rightManager = db.collection('aboutus').doc('manager2');
  rightManager.update({desc: document.getElementById("management-right-textinput").value})
}

function updateLeftManagerImg(files) {
  const storageRef = firebase.storage().ref();
  const leftImgRef = storageRef.child('user1.png');
  const file = files.item(0);
  const task = leftImgRef.put(file);
  task.then(snapshot => {
    const url = snapshot.downloadURL
    const leftManagerImg = db.collection('aboutus').doc('manager1');
    leftManagerImg.get() 
      .then(doc => {
        const data = doc.data();
        leftManagerImg.update({image: url})
        document.getElementById("management-left-image").src = data.image;
      })
  })
}

function updateRightManagerImg(files) {
  const storageRef = firebase.storage().ref();
  const rightImgRef = storageRef.child('user2.png');
  const file = files.item(0);
  const task = rightImgRef.put(file);
  task.then(snapshot => {
    const url = snapshot.downloadURL
    const rightManagerImg = db.collection('aboutus').doc('manager2');
    rightManagerImg.get() 
      .then(doc => {
        const data = doc.data();
        rightManagerImg.update({image: url})
        document.getElementById("management-right-image").src = data.image;
      })
  })
}
function uploadImg1(files) {
  const storageRef = firebase.storage().ref();
  const image1Ref = storageRef.child('roofing1.jpg');
  const file = files.item(0);
  const task = image1Ref.put(file);
  task.then(snapshot => {
    const url = snapshot.downloadURL
    const images = db.collection('homepage').doc('images');
    images.get() 
      .then(doc => {
        const data = doc.data();
        images.update({firstimage: url})
        document.getElementById("homepage-img1").src = data.firstimage;
      })
  })
}

function uploadImg2(files) {
  const storageRef = firebase.storage().ref();
  const image2Ref = storageRef.child('roofing2.jpg');
  const file = files.item(0);
  const task = image2Ref.put(file);
  task.then(snapshot => {
    const url = snapshot.downloadURL
    const images = db.collection('homepage').doc('images');
    images.get() 
      .then(doc => {
        const data = doc.data();
        images.update({secondimage: url})
        document.getElementById("homepage-img2").src = data.secondimage;
      })
  })
}

function uploadImg3(files) {
  const storageRef = firebase.storage().ref();
  const image3Ref = storageRef.child('roofing3.jpg');
  const file = files.item(0);
  const task = image3Ref.put(file);
  task.then(snapshot => {
    const url = snapshot.downloadURL
    const images = db.collection('homepage').doc('images');
    images.get() 
      .then(doc => {
        const data = doc.data();
        images.update({thirdimage: url})
        document.getElementById("homepage-img3").src = data.thirdimage;
      })
  })
}

function updateDesc() {
  const db = firebase.firestore();
  const desc = db.collection('homepage').doc('description');
  desc.update({desc: document.getElementById("description-text").value})
}

function updateQuote() {
  const db = firebase.firestore();
  const quote = db.collection('homepage').doc('quote');
  quote.update({quotetext: document.getElementById("quote-text").value})
}

function updateQuoteAuth() {
  const db = firebase.firestore();
  const quote = db.collection('homepage').doc('quote');
  quote.update({quoteauthor: document.getElementById("quote-auth").value})
}

function updateQuoteComp() {
  const db = firebase.firestore();
  const quote = db.collection('homepage').doc('quote');
  quote.update({quotecomp: document.getElementById("quote-comp").value})
}

