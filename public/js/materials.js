document.addEventListener("DOMContentLoaded", event => {
    const app = firebase.app();
    const db = firebase.firestore();
    var title = document.title;

    if (title != "Admin Page") {
        db.collection('materials').get()
          .then((snapshot) => {
            snapshot.docs.forEach(doc => {
              renderMaterials(doc);
          })
        })
      }
});
var title = document.title;
const materials = document.getElementById("materialsNav");
const materialContent = document.getElementById("materialsContent");
if (title != "Admin Page") {
    var firstEl = true;
    
    function renderMaterials(doc){
      let button = document.createElement('button');
      button.classList.add("materials-button");
      let div = document.createElement('div');
      div.classList.add("section__material");
      div.classList.add('hidden');
      if (firstEl == true) {
          button.classList.add("active");
          div.classList.remove('hidden');
          firstEl = false;
      }
      let desc = document.createElement('p');
      let image = document.createElement("IMG");
      image.addEventListener('mouseover', (e) => {
        image.classList.add('zoom-img');
      })
      image.addEventListener('mouseout', (e) => {
        image.classList.remove('zoom-img');
      })
      button.setAttribute('data-id', doc.id);
      div.id = button.getAttribute('data-id');
      button.textContent = doc.data().name;
      desc.textContent = doc.data().desc;
      image.src = doc.data().image;
      image.classList.toggle("materials-image");
      div.appendChild(image);
      div.appendChild(desc);
    
      materials.appendChild(button);
      materialContent.appendChild(div);

      button.addEventListener('click', (e) => {
        let id = e.target.getAttribute('data-id');
        var c = materialContent.children;
        var i;
        for (i = 0; i < c.length; i++) {
            c[i].classList.add("hidden");
        }
        var b = materials.children;
        var j;
        for (j = 0; j < b.length; j++) {
            b[j].classList.remove("active");
        }
        const target = document.getElementById(id);
        target.classList.remove('hidden');
        button.classList.add("active");
    })
    }
  }

$(document).ready(() => {
    const bodyContainer = $("body");
    const headerContainer = $(".header");
    const headerMenuBtn = document.getElementsByClassName("js-header-menu-toggle")[0];
    const activeClass = "is-open";
    var title = document.title;

    if (title == "Materials") {
      document.getElementById("nav5").classList.add("active");
    }

  
    if (title != "Admin Page") {
        let isMenuOpen = false;
        headerMenuBtn.addEventListener("click", () => {
          if (isMenuOpen) {
            bodyContainer.removeClass("is-scroll-disabled");
            headerContainer.removeClass(activeClass);
            isMenuOpen = false;
          } else {
            bodyContainer.addClass("is-scroll-disabled");
            headerContainer.addClass(activeClass);
            isMenuOpen = true;
          }
        })
      
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })
      }
  });