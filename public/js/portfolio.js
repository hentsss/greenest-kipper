document.addEventListener("DOMContentLoaded", event => {
    const app = firebase.app();
    const db = firebase.firestore();
    var title = document.title;
  if (title != "Admin Page") {
    db.collection('projects').orderBy('date').get()
      .then((snapshot) => {
        snapshot.docs.forEach(doc => {
          renderProject(doc);
      })
    })
  }
  });
  var title = document.title;
  const residentialProjects = document.getElementById("residential-container");
  const commercialProjects = document.getElementById("commercial-container");
  const minesProjects = document.getElementById("mines-container");
  const projects = document.getElementById("projectsList");

  if (title != "Admin Page") {
    
    function renderProject(doc){
      let div = document.createElement('div');
      let name = document.createElement('h2');
      let date = document.createElement('p');
      let materials = document.createElement('p');
      let systems = document.createElement('p');
      let techs = document.createElement('p');
      let image = document.createElement("IMG");
    
      div.setAttribute('data-id', doc.id);
      name.textContent = doc.data().name;
      date.textContent = "Date: " + doc.data().date;
      materials.textContent = "Used materials: " + doc.data().materials;
      systems.textContent = "Used systems: " + doc.data().systems;
      techs.textContent = "Used technologies: " + doc.data().techs;
      image.src = doc.data().image;
      image.classList.toggle("project-image");
    
      div.appendChild(name);
      div.appendChild(date);
      div.appendChild(materials);
      div.appendChild(systems);
      div.appendChild(techs);
      div.appendChild(image);
    
      if (doc.data().type == "residential") {
        residentialProjects.prepend(div);
      }
    
      if (doc.data().type == "commercial") {
        commercialProjects.prepend(div);
      }
    
      if (doc.data().type == "mines") {
        minesProjects.prepend(div);
      }
    }
  }
  

  $(document).ready(() => {
    const bodyContainer = $("body");
    const headerContainer = $(".header");
    const headerMenuBtn = document.getElementsByClassName("js-header-menu-toggle")[0];
    const activeClass = "is-open";
    var title = document.title;

    if (title == "Portfolio") {
      document.getElementById("nav6").classList.add("active");
    }

  
    if (title != "Admin Page") {
        let isMenuOpen = false;
        headerMenuBtn.addEventListener("click", () => {
          if (isMenuOpen) {
            bodyContainer.removeClass("is-scroll-disabled");
            headerContainer.removeClass(activeClass);
            isMenuOpen = false;
          } else {
            bodyContainer.addClass("is-scroll-disabled");
            headerContainer.addClass(activeClass);
            isMenuOpen = true;
          }
        })
      
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })
      }
  });