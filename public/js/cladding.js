document.addEventListener("DOMContentLoaded", event => {
    const app = firebase.app();
    const db = firebase.firestore();
    var title = document.title;

    if (title != "Admin Page") {
      db.collection('cladding').get()
        .then((snapshot) => {
          snapshot.docs.forEach(doc => {
            renderCladdingSols(doc);
        })
      })
    }
});

var title = document.title;
const accordion = document.getElementById("cladding-accordion");
if (title != "Admin Page") {

  function renderCladdingSols(doc){
    let button = document.createElement('button');
    button.classList.add("accordion");

    let header = document.createElement('div');
    header.classList.add("accordion-header");

    let caret_div = document.createElement('div');
    caret_div.classList.add('accordion-caret');

    let caret = document.createElement('IMG');
    caret.src = "assets/images/caret_down.png";

    let panel = document.createElement('div');
    panel.classList.add('panel');

    let content = document.createElement('p');
    let pictures = document.createElement('div');
    pictures.classList.add('accordion__card-pictures');
    let image1 = document.createElement('IMG');
    let image2 = document.createElement('IMG');
    let image3 = document.createElement('IMG');
    image1.classList.add('accordion__card-pictures-image');
    image2.classList.add('accordion__card-pictures-image');
    image3.classList.add('accordion__card-pictures-image');

    button.setAttribute('data-id', doc.id);
    header.innerHTML = doc.data().title;
    content.innerHTML = doc.data().content;
    image1.src = doc.data().img1;
    image2.src = doc.data().img2;
    image3.src = doc.data().img3;

    button.appendChild(header);
    caret_div.appendChild(caret);
    button.appendChild(caret_div);
    panel.appendChild(content);
    pictures.appendChild(image1);
    pictures.appendChild(image2);
    pictures.appendChild(image3);
    panel.appendChild(pictures);

    button.addEventListener("click", function() {
      /* Toggle between adding and removing the "active" class,
      to highlight the button that controls the panel */
      this.classList.toggle("accActive");
  
      /* Toggle between hiding and showing the active panel */
      var panel = this.nextElementSibling;
      if (panel.style.display === "block") {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
    });

    accordion.appendChild(button);
    accordion.appendChild(panel);
  }
}

$(document).ready(() => {
    const bodyContainer = $("body");
    const headerContainer = $(".header");
    const headerMenuBtn = document.getElementsByClassName("js-header-menu-toggle")[0];
    const activeClass = "is-open";
    const animation = document.querySelector('.animation');
    const hidden = $(".hidden");
    const materialBtn = $(".materials-button");
    var title = document.title;
    if (title == "Cladding solutions") {
      document.getElementById("nav4").classList.add("active");
    }
    if (title != "Admin Page") {
      let isMenuOpen = false;
      headerMenuBtn.addEventListener("click", () => {
        if (isMenuOpen) {
          bodyContainer.removeClass("is-scroll-disabled");
          headerContainer.removeClass(activeClass);
          isMenuOpen = false;
        } else {
          bodyContainer.addClass("is-scroll-disabled");
          headerContainer.addClass(activeClass);
          isMenuOpen = true;
        }
      })
    
      $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      })
    }
  });