document.addEventListener("DOMContentLoaded", event => {
    const app = firebase.app();
    const db = firebase.firestore();

      const aboutDesc = db.collection('aboutus').doc('description');
      aboutDesc.get()
        .then(doc => {
          const data = doc.data();
          document.getElementById("about-desc").innerHTML = data.desc;
        })
  
        const miningDesc = db.collection('aboutus').doc('mining');
        miningDesc.get()
          .then(doc => {
            const data = doc.data();
            document.getElementById("mining-desc").innerHTML = data.desc;
          })
  
        const leftManager = db.collection('aboutus').doc('manager1');
        leftManager.get()
          .then(doc => {
            const data = doc.data();
            document.getElementById("management-left-text").innerHTML = data.desc;
          })
  
        const rightManager = db.collection('aboutus').doc('manager2');
        rightManager.get()
          .then(doc => {
            const data = doc.data();
            document.getElementById("management-right-text").innerHTML = data.desc;
          })
  
        const manager1Img = db.collection('aboutus').doc('manager1');
        manager1Img.get() 
          .then(doc => {
            const data = doc.data();
            document.getElementById("management-left-image").src = data.image;
          })
  
        const manager2Img = db.collection('aboutus').doc('manager2');
        manager2Img.get() 
          .then(doc => {
            const data = doc.data();
            document.getElementById("management-right-image").src = data.image;
          })

});

$(document).ready(() => {
    const bodyContainer = $("body");
    const headerContainer = $(".header");
    const headerMenuBtn = document.getElementsByClassName("js-header-menu-toggle")[0];
    const activeClass = "is-open";
    const animation = document.querySelector('.animation');
    const hidden = $(".hidden");
    const materialBtn = $(".materials-button");
    var title = document.title;
  
    if (title == "About us") {
      document.getElementById("nav2").classList.add("active");
    }
  
    if (title != "Admin Page") {
      let isMenuOpen = false;
      headerMenuBtn.addEventListener("click", () => {
        if (isMenuOpen) {
          bodyContainer.removeClass("is-scroll-disabled");
          headerContainer.removeClass(activeClass);
          isMenuOpen = false;
        } else {
          bodyContainer.addClass("is-scroll-disabled");
          headerContainer.addClass(activeClass);
          isMenuOpen = true;
        }
      })
    
      $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      })
    }
  });