document.addEventListener("DOMContentLoaded", event => {
    const app = firebase.app();
    const db = firebase.firestore();
    var title = document.title;
});

const form = document.getElementById("contact-form");
form.addEventListener('submit', (e) => {
    e.preventDefault();
    var name = form.name.value;
    var topic = form.topic.value;
    var email = form.email.value;
    var message = form.message.value;

    saveContactInfo(name, topic, email, message);
    form.reset();
}) 

function saveContactInfo(name, topic, email, message) {
    const db = firebase.firestore();
    db.collection("contact").add({
        to: "hannes@greenestroofing.com.au",
        message: {
            subject: name + " " + topic,
            from: email,
            text: "Sender: " + email + "\n\n" + message
        }
    })
}

$(document).ready(() => {
    const bodyContainer = $("body");
    const headerContainer = $(".header");
    const headerMenuBtn = document.getElementsByClassName("js-header-menu-toggle")[0];
    const activeClass = "is-open";
    var title = document.title;

    if (title == "Contacts") {
      document.getElementById("nav7").classList.add("active");
    }

  
    if (title != "Admin Page") {
        let isMenuOpen = false;
        headerMenuBtn.addEventListener("click", () => {
          if (isMenuOpen) {
            bodyContainer.removeClass("is-scroll-disabled");
            headerContainer.removeClass(activeClass);
            isMenuOpen = false;
          } else {
            bodyContainer.addClass("is-scroll-disabled");
            headerContainer.addClass(activeClass);
            isMenuOpen = true;
          }
        })
      
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })
      }
  });