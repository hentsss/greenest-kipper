document.addEventListener("DOMContentLoaded", event => {
    var title = document.title;
    const app = firebase.app();
    const db = firebase.firestore();

    const images = db.collection('homepage').doc('images');
    images.get() 
    .then(doc => {
      const data = doc.data();
      document.getElementById("homepage-img1").src = data.firstimage;
      document.getElementById("homepage-img2").src = data.secondimage;
      document.getElementById("homepage-img3").src = data.thirdimage;
    })
    
    const residentialProject = document.getElementById("resFirst");
    const commercialProject = document.getElementById("commFirst");
    const minesProject = document.getElementById("mineFirst");

    if (title != "Admin Page") {
      db.collection('projects').where("type", "==", "residential").orderBy('date').limitToLast(1).get()
        .then((snapshot) => {
          snapshot.docs.forEach(doc => {
            const data = doc.data();
            residentialProject.src = doc.data().image;
        })
      })

      db.collection('projects').where("type", "==", "commercial").orderBy('date').limitToLast(1).get()
      .then((snapshot) => {
        snapshot.docs.forEach(doc => {
          const data = doc.data();
          commercialProject.src = data.image;
      })
    })

      db.collection('projects').where("type", "==", "mines").orderBy('date').limitToLast(1).get()
      .then((snapshot) => {
        snapshot.docs.forEach(doc => {
          const data = doc.data();
          minesProject.src = doc.data().image;
    })
  })
    }

    const desc = db.collection('homepage').doc('description');
    desc.get()
      .then(doc => {
        const data = doc.data();
        document.getElementById("homepage-desc").innerHTML = data.desc;
      })
  
    const quote = db.collection('homepage').doc('quote');
    quote.get()
      .then(doc => {
        const data = doc.data();
        document.getElementById("homepage-quote").innerHTML = data.quotetext;
        document.getElementById("homepage-quote-auth").innerHTML = data.quoteauthor;
        document.getElementById("homepage-quote-comp").innerHTML = data.quotecomp;
      })

});

  $(document).ready(() => {
    const bodyContainer = $("body");
    const headerContainer = $(".header");
    const headerMenuBtn = document.getElementsByClassName("js-header-menu-toggle")[0];
    const activeClass = "is-open";
    const animation = document.querySelector('.animation');
    const hidden = $(".hidden");
    const materialBtn = $(".materials-button");
    var title = document.title;
  
    if (title == "Greenest Roofing Solutions") {
        animation.addEventListener('animationend', () => {
          animation.classList.add("hidden");});
        document.getElementById("nav1").classList.add("active");
    }
    if (title != "Admin Page") {
      let isMenuOpen = false;
      headerMenuBtn.addEventListener("click", () => {
        if (isMenuOpen) {
          bodyContainer.removeClass("is-scroll-disabled");
          headerContainer.removeClass(activeClass);
          isMenuOpen = false;
        } else {
          bodyContainer.addClass("is-scroll-disabled");
          headerContainer.addClass(activeClass);
          isMenuOpen = true;
        }
      })
    
      $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      })
    }
  });